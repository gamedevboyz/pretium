﻿using UnityEngine;

public class Interactable : MonoBehaviour {
	public float radius = 3f;

	bool isFocus = false;
	Transform player;

	bool interacted = false;

	public virtual void Interact(){
		Debug.Log ("A hordó üres");
	}

	void Update(){
		if (isFocus  && !interacted) {
			float distance = Vector3.Distance (player.position, transform.position);
			if (distance <= radius) {
				Interact ();
				interacted = true;
			}
		}
	}

	public void OnFocused(Transform playerTransform){
		isFocus = true;
		player = playerTransform;
		interacted = false;
	}

	public void onDefocused(){
		isFocus = false;
		player = null;
		interacted = false;
	}

	void OnDrawGizmosSelected (){
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere (transform.position, radius);
	}
}
